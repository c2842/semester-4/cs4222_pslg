package Week_03.Group_2;

public class Circle {
  // every circle is defined by its radius
  double radius;

  public Circle(){
    // set default radius to 10
    radius = 10;
  }

  public Circle(double newRadius){
    radius = newRadius;
  }

  public double area(){
    return 3.14 * radius * radius;
  }

  public double circumference(){
    return 2 * 3.14 * radius;
  }
}



