package Week_03.Group_2;

public class CircleRunner {

  public static void main(String[] args) {
    Circle c1 = new Circle();
    Circle c2 = new Circle(5);

    System.out.printf("Radius: %.2f, Area: %.2f, Circumference %.2f %n",
        c1.radius,
        c1.area(),
        c1.circumference()
    );
    System.out.printf("Radius: %.2f, Area: %.2f, Circumference %.2f %n",
        c2.radius,
        c2.area(),
        c2.circumference()
    );
  }
}
