package Week05;

import java.util.ArrayList;
import java.util.Arrays;

public class Week05ArrayLists {
    public static void main(String[] args) {
        // Declaring ArrayLists
        ArrayList<Integer> blankList = new ArrayList<>();
        ArrayList<Double> blankList1 = new ArrayList<>();
        // Prefilling it with values
        ArrayList<Integer> blankList2 = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8));

        // Exercise 1
        for (int i = 0; i < 10; i++) {
            // Reusing the lists we made earlier
            blankList.add(i);
        }
        System.out.println(blankList);

        // Exercise 2
        for (int i = 0; i < blankList.size(); i++) {
            if (blankList.get(i) % 2 != 0) {
                // Q: What's the problem here?
                blankList.remove(i);
            }
        }
        System.out.println(blankList);
    }
}
