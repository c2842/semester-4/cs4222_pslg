package Week_11.Group_02;

import java.util.HashMap;

public class HashMap_example {
    public static void main(String[] args) {
        HashMap<String, String> tmp = new HashMap<>();

        tmp.put("Sky full of stars", "Coldplay");
        tmp.put("As it was", "Harry Styles");
        tmp.put("take on me", "A-HA");

        // cheeky I know
        for(String key: tmp.keySet()){
            System.out.printf("{} {} \n", key, tmp.get(key));
        }
    }


}
