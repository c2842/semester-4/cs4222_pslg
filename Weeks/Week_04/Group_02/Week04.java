package Week04;

public class Week04 {
    public static void main(String[] args) {

        //How to declare 2D arrays

        // Just a direct declaration
        int[][] int2D = new int[2][2];
        // Declaring with values, note the curly brackets on the inside
        double[][] doubles2D = {{1.0, 2.0, 3.0}, {4.0, 5.0, 6.0}, {7.0, 8.0, 9.0}};
        // For the for loop example
        double[][] doubles2D_2 =  {
                {1.0, 2.0, 3.0},
                {4.0, 5.0, 6.0},
                {7.0, 8.0, 9.0}
        };
        // Printing out the double array using a for loop
        for(int i = 0; i < doubles2D_2.length; i++){
            for(int j = 0; j < doubles2D_2[0].length; j++){
                System.out.println(doubles2D[i][j]);
            }
        }
        // For the enhanced for loop example
        double[][] doubles2D_3 =  {
                {1.0, 2.0, 3.0},
                {4.0, 5.0, 6.0},
                {7.0, 8.0, 9.0}
        };
        // Using the enhanced for loops
        for(double[] innerArr: doubles2D_3) {
            for (double v : innerArr) {
                System.out.println(v);
            }
        }
    }
}
