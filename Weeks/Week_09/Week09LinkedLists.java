package Week09;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Arrays;
public class Week09LinkedLists {

    public static void main(String[] args) {
        LinkedList<Integer> linkedList = new LinkedList<>();
        LinkedList<Double> doubles = new LinkedList<Double>();
        LinkedList<Integer> filledList = new LinkedList<>(
          Arrays.asList(1,2,3,4,5,6,7,8)
        );

        for (Integer integer : filledList) {
            System.out.println(integer);
        }

        for(int i = filledList.size() - 1; i > 0; i--){
            System.out.println(filledList.get(i));
        }
        ArrayList<LinkedList<Integer>> chaos = new ArrayList<>();
        for(int i = 0; i < 5; i++){
            chaos.add(new LinkedList<>());
        }
        for(int i = 0; i < 5; i++) {
            chaos.get(i).add(i);
        }
        for(int i = 0; i < chaos.size(); i++){
            for(int j = 0; j < chaos.get(i).size(); j++){
                System.out.println(chaos.get(i).get(j));
            }
        }
    }

}
