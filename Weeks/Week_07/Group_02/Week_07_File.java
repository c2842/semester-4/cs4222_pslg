package Week_07.Group_02;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Scanner;

public class Week_07_File {
    public static void main(String[] args) throws FileNotFoundException {
        // get a file handler
        file();
        // read the input (System.in in this case)
        scanner();
        // use both File and Scanner to read the file.
        reader();
    }

    private static void file(){
        String path = "C:\\Users\\23sil\\Desktop\\PSLG_2022_week07.txt";
        File tmp = new File(path);
        System.out.printf(
            """
            Exists  : %b
            Readable: %b
            Name    : %s
            Length  : %d
            """, tmp.exists(), tmp.canRead(), tmp.getName(), tmp.length());
    }

    private static void scanner() {
        // start the scanner object
        Scanner scan = new Scanner(System.in);
        while (scan.hasNextLine()) {
            // grab teh next line
            String line = scan.nextLine();

            // end condition for the while loop
            if (line.equals("STOP")) { break; }

            // echo back what is typed in
            System.out.printf("%s%n", line);
        }
    }

    private static void reader() throws FileNotFoundException {
        String path = "C:\\Users\\23sil\\Desktop\\PSLG_2022_week07.txt";
        File tmp = new File(path);
        Scanner scan = new Scanner(tmp);
        while (scan.hasNextLine()) {
            // grab teh next line
            String line = scan.nextLine();

            // end condition for the while loop
            if (line.equals("STOP")) { break; }

            // echo back what is typed in
            System.out.printf("%s%n", line);
        }
    }
}


