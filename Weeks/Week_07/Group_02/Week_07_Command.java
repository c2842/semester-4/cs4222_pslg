package Week_07.Group_02;

public class Week_07_Command {
    public static void main(String[] args) {
        // dont run if less than 3 arguments
        if (args.length < 3) {
            return;
        }
        String name = args[0];
        int age = Integer.parseInt(args[1], 10);
        double height = Double.parseDouble(args[2]);

        System.out.printf(
            """
            Name      : %s
            Age +12   : %d
            Height /2 : %.2f
            """,
            name, age + 12, height / 2
        );
    }
}


